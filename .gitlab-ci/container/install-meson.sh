#!/usr/bin/env bash

# We need at least 1.4.0 for rusticl
pip3 install --break-system-packages 'meson==1.4.0'
